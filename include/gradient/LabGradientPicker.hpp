/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LABGRADIENTPICKER_HPP
#define LABGRADIENTPICKER_HPP

#include <QColor>
#include <QMouseEvent>
#include <QPainter>
#include <QSlider>
#include <QVector3D>
#include <QWidget>

#include "ColorUtils.hpp"

class LabGradientPicker : public QWidget
{
	Q_OBJECT

  public:
	LabGradientPicker(unsigned int minL, unsigned int maxL,
	                  QWidget* parent = nullptr);
	LabGradientPicker(grd::Gradient const& initGradient,
	                  QWidget* parent = nullptr);
	grd::Gradient selectedGradient() const { return m_selectedGradient; };

  public slots:
	void setMinL(unsigned int minL);
	void setMaxL(unsigned int maxL);

  signals:
	void gradientChanged(grd::Gradient const& gradient);

  protected:
	void paintEvent(QPaintEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;

  private slots:
	QPoint getSelectedPosition(grd::Color const& color) const;
	QPoint getSelectedPosition(QVector2D const& control) const;

  private:
	void updateGradientFromMouse(QPoint const& pos);

	grd::Gradient m_selectedGradient;

	QSlider* m_lSlider;

	grd::Color* movedColor  = nullptr;
	QVector2D* movedControl = nullptr;
};

#endif // LABGRADIENTPICKER_HPP
