/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef COLORUTILS_HPP
#define COLORUTILS_HPP

#include <QColor>
#include <QLatin1String>
#include <QVector2D>
#include <QVector3D>

#include "gl/GLShaderProgram.hpp"

namespace grd
{

QVector2D bezierInterpolation(QVector2D const& p0, QVector2D const& p1,
                              QVector2D const& c0, QVector2D const& c1,
                              float t);

QVector3D linearRGBToLab(QVector3D const& rgb);
QVector3D labToLinearRGB(QVector3D const& lab);

QVector3D linearTosRGB(QVector3D const& linear);
QVector3D sRGBToLinear(QVector3D const& sRGB);

class Color
{
  public:
	enum class Space
	{
		SRGB,   // float [0; 1]
		RGB,    // float [0; 1]
		HSL,    // float [0; 1]
		CIELAB, // float ([0; 100], [-128;127], [-128, 127])
	};

	Color() = default;
	// NOLINTNEXTLINE(clang-diagnostic-deprecated-copy)
	Color(Color const&) = default;
	Color(QColor const&);
	Color(QVector3D const& values, Space space);
	Color(QJsonObject const& json) { setJson(json); };
	Space getSpace() const { return space; };
	QVector3D getAs(Space space) const;
	QColor getAsQColor() const;
	void convertTo(Space space);
	void set(QVector3D const& values, Space space);
	QJsonObject getJson() const;
	void setJson(QJsonObject const& json);

	// private:
	QVector3D values;
	Space space = Space::SRGB;
};

struct Gradient
{
	enum class Type
	{
		LINEAR_SRGB,
		LINEAR_RGB,
		LINEAR_HSL,
		LINEAR_CIELAB,
		BEZIER_CIELAB,
	};

	Gradient() = default;
	Gradient(QJsonObject const& json) { setJson(json); };

	Color start = {QVector3D(15, 48.0272, -61.9574), grd::Color::Space::CIELAB};
	QVector2D c1 = {65.8472, -34.7234}; // for CIELAB bezier interpolation
	QVector2D c2 = {56.7199, 82.383};   // for CIELAB bezier interpolation
	Color end    = {QVector3D(84, 3.6944, 82.383), grd::Color::Space::CIELAB};
	Type type    = Type::BEZIER_CIELAB;

	bool grey    = false;
	float minVal = 0.f;
	float maxVal = 100.f;
	bool useLog  = false;

	QJsonObject getJson() const;
	void setJson(QJsonObject const& json);

	void setShaderUniforms(GLShaderProgram const& shader) const;
};

} // namespace grd

#endif // COLORUTILS_HPP
