uniform vec3 startColor;
uniform vec2 c1;
uniform vec2 c2;
uniform vec3 endColor;
uniform int interpolationType = 0;
uniform float useGrey         = 0.0;

uniform vec2 limits  = vec2(0.0, 1.0);
uniform float useLog = 0.0;

#include <gradient/color.glsl>

vec2 bezierInterpolation(vec2 p0, vec2 p1, vec2 c0, vec2 c1, float t)
{
	float u   = 1.0 - t;
	float tt  = t * t;
	float uu  = u * u;
	float uuu = uu * u;
	float ttt = tt * t;

	vec2 p = uuu * p0;
	p += 3.0 * uu * t * c0;
	p += 3.0 * u * tt * c1;
	p += ttt * p1;

	return p;
}

vec3 interpolateLinearsRGB(vec3 a, vec3 b, float t)
{
	return sRGBToLinear((1.0 - t) * a + t * b);
}

vec3 interpolateLinearRGB(vec3 a, vec3 b, float t)
{
	return (1.0 - t) * a + t * b;
}

vec3 interpolateLinearHSV(vec3 a, vec3 b, float t)
{
	return sRGBToLinear(HSLTosRGB((1.0 - t) * a + t * b));
}

vec3 interpolateLinearLab(vec3 a, vec3 b, float t)
{
	return LabToRGB((1.0 - t) * a + t * b);
}

vec3 interpolateBezierLab(vec3 a, vec3 b, float t)
{
	float L = (1.0 - t) * a.x + t * b.x;
	vec2 ab = bezierInterpolation(a.yz, b.yz, c1, c2, t);
	return LabToRGB(vec3(L, ab));
}

// returns linear RGB
vec3 evaluateGradient(float value, float yvalue)
{
	float t = (value - limits.x) / (limits.y - limits.x);
	if(useLog > 0.0)
	{
		t = (log(value) - log(limits.x)) / (log(limits.y) - log(limits.x));
	}
	t = clamp(t, 0.0, 1.0);

	vec3 grayscale = vec3(0.2126, 0.7152, 0.0722);
	vec3 result    = vec3(0.0);
	if(interpolationType == 0)
	{
		result = interpolateLinearsRGB(startColor, endColor, t);
	}
	else if(interpolationType == 1)
	{
		result = interpolateLinearRGB(startColor, endColor, t);
	}
	else if(interpolationType == 2)
	{
		result = interpolateLinearHSV(startColor, endColor, t);
	}
	else if(interpolationType == 3)
	{
		result = interpolateLinearLab(startColor, endColor, t);
	}
	else if(interpolationType == 4)
	{
		result = interpolateBezierLab(startColor, endColor, t);
		if(result.r < 0.0)
		{
			return fract(yvalue * 10.0) > 0.5 ? vec3(1.0, 0.0, 0.0)
			                                  : vec3(0.0, 1.0, 0.0);
		}

		float grey        = dot(grayscale, result);
		float desiredGrey = mix(startColor.x / 100.0, endColor.x / 100.0, t);
		desiredGrey       = pow(desiredGrey, 2.4);

		result *= desiredGrey / grey;
	}

	if(result.r < 0.0)
	{
		return fract(yvalue * 10.0) > 0.5 ? vec3(1.0, 0.0, 0.0)
		                                  : vec3(0.0, 1.0, 0.0);
	}

	vec3 newGrey = vec3(dot(grayscale, result));
	result       = mix(result, newGrey, useGrey);

	return result;
}

vec3 evaluateGradient(float value)
{
	return evaluateGradient(value, 0.0);
}

