
vec3 sRGBToLinear(vec3 srgb)
{
	return mix(srgb / 12.92, pow((srgb + 0.055) / 1.055, vec3(2.4)),
	           greaterThan(srgb, vec3(0.04045)));
}

vec3 linearTosRGB(vec3 linear)
{
	return mix(linear * 12.92, 1.055 * pow(linear, vec3(1.0 / 2.4)) - 0.055,
	           greaterThan(linear, vec3(0.0031308)));
}

float hue2rgb(float p, float q, float t)
{
	if(t < 0.0)
		t += 1.0;
	if(t > 1.0)
		t -= 1.0;
	if(t < 1.0 / 6.0)
		return p + (q - p) * 6.0 * t;
	if(t < 0.5)
		return q;
	if(t < 2.0 / 3.0)
		return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
	return p;
}

vec3 HSLTosRGB(vec3 hsl)
{
	float h = hsl.x;
	float s = hsl.y;
	float l = hsl.z;

	if(s == 0.0)
	{
		return vec3(l, l, l);
	}

	float q = (l < 0.5) ? (l * (1.0 + s)) : (l + s - l * s);
	float p = 2.0 * l - q;

	float r = hue2rgb(p, q, h + 1.0 / 3.0);
	float g = hue2rgb(p, q, h);
	float b = hue2rgb(p, q, h - 1.0 / 3.0);

	return vec3(r, g, b);
}

vec3 sRGBToHSL(vec3 rgb)
{
	float r = rgb.x;
	float g = rgb.y;
	float b = rgb.z;

	float maxVal = max(max(r, g), b);
	float minVal = min(min(r, g), b);
	float h, s, l;

	l = (maxVal + minVal) / 2.0;

	if(maxVal == minVal)
	{
		h = s = 0.0; // Achromatic
	}
	else
	{
		float d = maxVal - minVal;
		s = (l > 0.5) ? (d / (2.0 - maxVal - minVal)) : (d / (maxVal + minVal));

		if(maxVal == r)
		{
			h = (g - b) / d + ((g < b) ? 6.0 : 0.0);
		}
		else if(maxVal == g)
		{
			h = (b - r) / d + 2.0;
		}
		else
		{
			h = (r - g) / d + 4.0;
		}

		h /= 6.0;
	}

	return vec3(h, s, l);
}

vec3 f(vec3 t, float epsilon, float kappa)
{
	return mix(pow(t, vec3(1.0 / 3.0)), (t * kappa + 16.0) / 116.0,
	           lessThan(t, vec3(epsilon)));
}

vec3 fInverse(vec3 t, float epsilon, float kappa)
{
	return mix(pow(t, vec3(3.0)), (116.0 * t - 16.0) / kappa,
	           lessThan(t, vec3(epsilon / kappa)));
}

vec3 LinearRGBToXYZ(vec3 color)
{
	mat3 RGB2XYZ = mat3(0.4124564, 0.3575761, 0.1804375, 0.2126729, 0.7151522,
	                    0.0721750, 0.0193339, 0.1191920, 0.9503041);
	return RGB2XYZ * color;
}

vec3 XYZToLab(vec3 color)
{
	vec3 refWhite   = vec3(0.95047, 1.0, 1.08883);
	vec3 normalized = color / refWhite;

	const float epsilon = 0.008856;
	const float kappa   = 903.3;

	vec3 normalizedF = f(normalized, epsilon, kappa);

	float L = 116.0 * normalizedF.y - 16.0;
	float a = 500.0 * (normalizedF.x - normalizedF.y);
	float b = 200.0 * (normalizedF.y - normalizedF.z);

	return vec3(L, a, b);
}

vec3 RGBToLab(vec3 color)
{
	return XYZToLab(LinearRGBToXYZ(color));
}

vec3 LabToRGB(vec3 lab)
{
	// Convert Lab to XYZ
	float y = (lab.x + 16.0) / 116.0;
	float x = lab.y / 500.0 + y;
	float z = y - lab.z / 200.0;

	x = 0.95047
	    * ((x * x * x > 0.008856) ? x * x * x : (x - 16.0 / 116.0) / 7.787);
	y = 1.00000
	    * ((y * y * y > 0.008856) ? y * y * y : (y - 16.0 / 116.0) / 7.787);
	z = 1.08883
	    * ((z * z * z > 0.008856) ? z * z * z : (z - 16.0 / 116.0) / 7.787);

	// Convert XYZ to RGB

	float r  = x * 3.2406 + y * -1.5372 + z * -0.4986;
	float g  = x * -0.9689 + y * 1.8758 + z * 0.0415;
	float bl = x * 0.0557 + y * -0.2040 + z * 1.0570;

	// Clamp and return the result
	if(r < 0.0 || r > 1.0 || g < 0.0 || g > 1.0 || bl < 0.0 || bl > 1.0)
	{
		return vec3(-1.0, -1.0, -1.0);
	}
	return vec3(r, g, bl);
}
