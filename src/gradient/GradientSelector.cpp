/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gradient/GradientSelector.hpp"

#include <QCheckBox>
#include <QFormLayout>
#include <QLabel>
#include <QSlider>
#include <cmath>
#include <numbers>

GradientSelector::GradientSelector(std::unique_ptr<grd::Gradient> gradient,
                                   QWidget* parent)
    : GradientSelector(*gradient, parent)
{
	ownedGradient = std::move(gradient);
}

GradientSelector::GradientSelector(grd::Gradient& gradient, QWidget* parent)
    : QWidget(parent)
    , gradient(gradient)
    , gradientDisplay(gradient, this)
    , tabs(this)
    , startSelector(tr("Start color"), this)
    , endSelector(tr("End color"), this)
    , gradTypeSelector(this)
    , gradientPicker(gradient, this)
    , minLSelector(Qt::Horizontal, this)
    , maxLSelector(Qt::Horizontal, this)
    , greyScale(this)
    , minValSelector(this)
    , minExponentSelector(this)
    , maxValSelector(this)
    , maxExponentSelector(this)
    , useLogBox(this)
{
	connect(this, &GradientSelector::gradientChanged,
	        [this]() { gradientDisplay.update(); });

	auto* globalLayout(make_qt_unique<QVBoxLayout>(*this));

	globalLayout->addWidget(&gradientDisplay);
	globalLayout->addWidget(&tabs);
	auto* dataWidget(make_qt_unique<QWidget>(*this));
	globalLayout->addWidget(dataWidget);

	auto* dataLayout(make_qt_unique<QFormLayout>(*dataWidget));
	dataLayout->setSizeConstraint(QLayout::SetMaximumSize);

	dataLayout->addRow(tr("Gray scale:"), &greyScale);
	greyScale.setChecked(gradient.grey);
	connect(&greyScale, &QCheckBox::stateChanged,
	        [this](int s)
	        {
		        this->gradient.grey = s == Qt::Checked;
		        emit gradientChanged();
	        });

	auto exponent(floor(log(gradient.minVal) / std::numbers::ln10));
	if(gradient.minVal == 0.0)
	{
		exponent = 0.0;
	}
	if(gradient.minVal < 0.0)
	{
		exponent = floor(log(-gradient.minVal) / std::numbers::ln10);
	}
	auto mantissa(gradient.minVal / pow(10, exponent));
	minValSelector.setRange(-9.999, 9.999);
	minValSelector.setDecimals(3);
	minValSelector.setValue(mantissa);
	minValSelector.setSingleStep(0.1);
	dataLayout->addRow(tr("Minimum value :"), &minValSelector);
	connect(&minValSelector,
	        static_cast<void (QDoubleSpinBox::*)(double)>(
	            &QDoubleSpinBox::valueChanged),
	        [this](double d)
	        {
		        const double val(d * pow(10, minExponentSelector.value()));
		        this->gradient.minVal = val;
		        emit gradientChanged();
	        });

	auto* w     = make_qt_unique<QWidget>(*this);
	auto* l     = make_qt_unique<QHBoxLayout>(*w);
	auto* label = make_qt_unique<QLabel>(*this);
	label->setText("x10^");
	l->addWidget(label);
	l->addWidget(&minExponentSelector);
	dataLayout->addRow("", w);

	minExponentSelector.setRange(-37, 37);
	minExponentSelector.setValue(exponent);
	connect(&minExponentSelector,
	        static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	        [this](int i)
	        {
		        const double val(minValSelector.value() * pow(10, i));
		        this->gradient.minVal = val;
		        emit gradientChanged();
	        });

	exponent = floor(log(gradient.maxVal) / std::numbers::ln10);
	if(gradient.maxVal == 0.0)
	{
		exponent = 0.0;
	}
	if(gradient.maxVal < 0.0)
	{
		exponent = floor(log(-gradient.maxVal) / std::numbers::ln10);
	}
	mantissa = gradient.maxVal / pow(10, exponent);
	maxValSelector.setRange(-9.999, 9.999);
	maxValSelector.setDecimals(3);
	maxValSelector.setValue(mantissa);
	maxValSelector.setSingleStep(0.1);
	dataLayout->addRow(tr("Maximum value :"), &maxValSelector);
	connect(&maxValSelector,
	        static_cast<void (QDoubleSpinBox::*)(double)>(
	            &QDoubleSpinBox::valueChanged),
	        [this](double d)
	        {
		        const double val(d * pow(10, maxExponentSelector.value()));
		        this->gradient.maxVal = val;
		        emit gradientChanged();
	        });

	w     = make_qt_unique<QWidget>(*this);
	l     = make_qt_unique<QHBoxLayout>(*w);
	label = make_qt_unique<QLabel>(*this);
	label->setText("x10^");
	l->addWidget(label);
	l->addWidget(&maxExponentSelector);
	dataLayout->addRow("", w);

	maxExponentSelector.setRange(-37, 37);
	maxExponentSelector.setValue(exponent);
	connect(&maxExponentSelector,
	        static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
	        [this](int i)
	        {
		        const double val(maxValSelector.value() * pow(10, i));
		        this->gradient.maxVal = val;
		        emit gradientChanged();
	        });

	dataLayout->addRow(tr("Log Scale :"), &useLogBox);
	useLogBox.setChecked(gradient.useLog);
	connect(&useLogBox, &QCheckBox::stateChanged,
	        [this](int s)
	        {
		        this->gradient.useLog = s == Qt::Checked;
		        emit gradientChanged();
	        });

	// Page 1

	const bool bezier(gradient.type == grd::Gradient::Type::BEZIER_CIELAB);

	auto* page1(make_qt_unique<QWidget>(*this));
	auto* page1Layout(make_qt_unique<QFormLayout>(*page1));
	page1Layout->addRow(tr("Start color : "), &startSelector);
	page1Layout->addRow(tr("End color : "), &endSelector);
	page1Layout->addRow(tr("Interp. type: "), &gradTypeSelector);

	startSelector.setColor(bezier ? QColor(33, 33, 255)
	                              : gradient.start.getAsQColor());
	connect(&startSelector, &ColorSelector::colorChanged,
	        [this](QColor const& c)
	        {
		        this->gradient.start = c;
		        emit gradientChanged();
	        });

	endSelector.setColor(bezier ? QColor(255, 33, 33)
	                            : gradient.end.getAsQColor());
	connect(&endSelector, &ColorSelector::colorChanged,
	        [this](QColor const& c)
	        {
		        this->gradient.end = c;
		        emit gradientChanged();
	        });

	gradTypeSelector.addItems({"sRGB", "RGB", "HSL", "CIELAB"});
	gradTypeSelector.setCurrentIndex(bezier ? 2
	                                        : static_cast<int>(gradient.type));
	connect(&gradTypeSelector, &QComboBox::currentTextChanged,
	        [this](QString const& s)
	        {
		        if(s == "sRGB")
		        {
			        this->gradient.type = grd::Gradient::Type::LINEAR_SRGB;
		        }
		        else if(s == "RGB")
		        {
			        this->gradient.type = grd::Gradient::Type::LINEAR_RGB;
		        }
		        else if(s == "HSL")
		        {
			        this->gradient.type = grd::Gradient::Type::LINEAR_HSL;
		        }
		        else if(s == "CIELAB")
		        {
			        this->gradient.type = grd::Gradient::Type::LINEAR_CIELAB;
		        }
		        // NOLINTNEXTLINE(readability-misleading-indentation)
		        emit gradientChanged();
	        });

	tabs.addTab(page1, tr("Linear interpolations"));

	// Page 2
	auto* page2(make_qt_unique<QWidget>(*this));
	auto* page2Layout(make_qt_unique<QVBoxLayout>(*page2));
	page2Layout->addWidget(&gradientPicker);
	connect(&gradientPicker, &LabGradientPicker::gradientChanged,
	        [this](grd::Gradient const& g)
	        {
		        this->gradient.start = g.start;
		        this->gradient.c1    = g.c1;
		        this->gradient.c2    = g.c2;
		        this->gradient.end   = g.end;
		        this->gradient.type  = g.type;
		        emit gradientChanged();
	        });

	auto* page2Sliders(make_qt_unique<QWidget>(*this));
	page2Layout->addWidget(page2Sliders);
	auto* page2SlidersLayout(make_qt_unique<QFormLayout>(*page2Sliders));

	minLSelector.setMinimum(0);
	minLSelector.setMaximum(100);
	minLSelector.setValue(20);
	page2SlidersLayout->addRow(tr("Minimum luminosity :"), &minLSelector);
	connect(&minLSelector, &QSlider::valueChanged, &gradientPicker,
	        &LabGradientPicker::setMinL);

	maxLSelector.setMinimum(0);
	maxLSelector.setMaximum(100);
	maxLSelector.setValue(80);
	page2SlidersLayout->addRow(tr("Maximum luminosity :"), &maxLSelector);
	connect(&maxLSelector, &QSlider::valueChanged, &gradientPicker,
	        &LabGradientPicker::setMaxL);

	tabs.addTab(page2, tr("Bezier La*b* interpolation"));

	connect(&tabs, &QTabWidget::currentChanged,
	        [this](int i)
	        {
		        if(i == 0)
		        {
			        const auto idx = gradTypeSelector.currentIndex();
			        gradTypeSelector.setCurrentIndex(0);
			        gradTypeSelector.setCurrentIndex(1);
			        gradTypeSelector.setCurrentIndex(idx);

			        this->gradient.start = startSelector.getCurrentColor();
			        this->gradient.end   = endSelector.getCurrentColor();
		        }
		        else
		        {
			        const auto g         = gradientPicker.selectedGradient();
			        this->gradient.start = g.start;
			        this->gradient.c1    = g.c1;
			        this->gradient.c2    = g.c2;
			        this->gradient.end   = g.end;
			        this->gradient.type  = g.type;
			        emit gradientChanged();
		        }
	        });

	tabs.setCurrentIndex(bezier ? 1 : 0);
}
