/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gradient/LabGradientPicker.hpp"

#include <QPainterPath>
#include <QVBoxLayout>
#include <cmath>

QPoint bezierInterpolation(QPoint const& p0, QPoint const& p1, QPoint const& c0,
                           QPoint const& c1, float t);

LabGradientPicker::LabGradientPicker(unsigned int minL, unsigned int maxL,
                                     QWidget* parent)
    : QWidget(parent)
{
	setMinimumSize(256, 256);
	// Set up the L* slider
	m_lSlider = make_qt_unique<QSlider>(*this, Qt::Horizontal);
	m_lSlider->setRange(minL, maxL);        // L* value range
	m_lSlider->setValue((minL + maxL) / 2); // Default L* value

	// Set up the layout
	auto* mainLayout = make_qt_unique<QVBoxLayout>(*this);
	mainLayout->addWidget(m_lSlider);
	mainLayout->addStretch();
	setLayout(mainLayout);

	// Connect the slider to update the L* value
	connect(m_lSlider, &QSlider::valueChanged, [this](int) { update(); });

	// ~= matplotlib plasma
	// Initialize the selected color
	m_selectedGradient.start.set(QVector3D(minL, 48.0272, -61.9574),
	                             grd::Color::Space::CIELAB);
	m_selectedGradient.c1 = {65.8472, -34.7234};
	m_selectedGradient.c2 = {56.7199, 82.383};
	m_selectedGradient.end.set(QVector3D(maxL, 3.6944, 82.383),
	                           grd::Color::Space::CIELAB);
	m_selectedGradient.type = grd::Gradient::Type::BEZIER_CIELAB;
}

LabGradientPicker::LabGradientPicker(grd::Gradient const& initGradient,
                                     QWidget* parent)
    : LabGradientPicker(15, 84, parent)
{
	if(initGradient.type != grd::Gradient::Type::BEZIER_CIELAB)
	{
		return;
	}
	m_selectedGradient.start = initGradient.start;
	m_selectedGradient.c1    = initGradient.c1;
	m_selectedGradient.c2    = initGradient.c2;
	m_selectedGradient.end   = initGradient.end;
}

void LabGradientPicker::setMinL(unsigned int minL)
{
	m_lSlider->setMinimum(minL);
	auto lab = m_selectedGradient.start.getAs(grd::Color::Space::CIELAB);
	lab[0]   = minL;
	m_selectedGradient.start.set(lab, grd::Color::Space::CIELAB);
	emit gradientChanged(m_selectedGradient);
	update();
}

void LabGradientPicker::setMaxL(unsigned int maxL)
{
	m_lSlider->setMaximum(maxL);
	auto lab = m_selectedGradient.end.getAs(grd::Color::Space::CIELAB);
	lab[0]   = maxL;
	m_selectedGradient.end.set(lab, grd::Color::Space::CIELAB);
	emit gradientChanged(m_selectedGradient);
	update();
}

void LabGradientPicker::paintEvent(QPaintEvent* event)
{
	QWidget::paintEvent(event);
	QPainter painter(this);

	// Draw the 2D representation of the a* and b* channels
	for(int x = 0; x < width(); x++)
	{
		for(int y = 0; y < height(); y++)
		{
			// Normalize the a* and b* values to fit within the widget
			const double a = (x / static_cast<double>(width())) * 256.0 - 128.0;
			const double b
			    = (y / static_cast<double>(height())) * 256.0 - 128.0;

			const grd::Color color(QVector3D(m_lSlider->value(), a, b),
			                       grd::Color::Space::CIELAB);
			const auto srgb(color.getAs(grd::Color::Space::SRGB));
			QColor qc(QColor::fromRgbF(srgb[0], srgb[1], srgb[2]));
			// if(qc.spec() == QColor::ExtendedRgb)
			if(srgb[0] < 0.0 || srgb[0] > 1.0 || srgb[1] < 0.0 || srgb[1] > 1.0
			   || srgb[2] < 0.0 || srgb[2] > 1.0)
			{
				qc = m_lSlider->value() >= 50 ? Qt::white : Qt::black;
			}
			painter.setPen(qc);
			painter.drawPoint(x, y);
		}
	}

	const QPoint start(getSelectedPosition(m_selectedGradient.start)),
	    c1(getSelectedPosition(m_selectedGradient.c1)),
	    c2(getSelectedPosition(m_selectedGradient.c2)),
	    end(getSelectedPosition(m_selectedGradient.end));

	QPen pen(m_lSlider->value() >= 50 ? Qt::black : Qt::white);
	pen.setWidth(2);
	painter.setPen(pen);
	painter.setBrush(Qt::NoBrush);
	painter.drawEllipse(start, 6, 6);
	painter.drawText(start - QPoint(4, 10), "S");
	painter.drawEllipse(c1, 6, 6);
	painter.drawText(c1 - QPoint(4, 10), "C1");
	painter.drawEllipse(c2, 6, 6);
	painter.drawText(c2 - QPoint(4, 10), "C2");
	painter.drawEllipse(end, 6, 6);
	painter.drawText(end - QPoint(4, 10), "E");

	QPainterPath path;
	path.moveTo(start);
	path.cubicTo(c1, c2, end);
	painter.drawPath(path);
	pen.setWidth(1);
	painter.setPen(pen);
	painter.drawLine(start, c1);
	painter.drawLine(end, c2);

	const float t
	    = static_cast<float>(m_lSlider->value() - m_lSlider->minimum())
	      / (m_lSlider->maximum() - m_lSlider->minimum());

	const auto crossPos = bezierInterpolation(start, end, c1, c2, t);
	painter.drawLine(crossPos + QPoint(6, 6), crossPos - QPoint(6, 6));
	painter.drawLine(crossPos + QPoint(6, -6), crossPos - QPoint(6, -6));
}

void LabGradientPicker::mousePressEvent(QMouseEvent* event)
{
	movedColor   = nullptr;
	movedControl = nullptr;
	if((getSelectedPosition(m_selectedGradient.start) - event->pos())
	       .manhattanLength()
	   < 12)
	{
		movedColor = &m_selectedGradient.start;
	}
	if((getSelectedPosition(m_selectedGradient.c1) - event->pos())
	       .manhattanLength()
	   < 12)
	{
		movedControl = &m_selectedGradient.c1;
	}
	if((getSelectedPosition(m_selectedGradient.c2) - event->pos())
	       .manhattanLength()
	   < 12)
	{
		movedControl = &m_selectedGradient.c2;
	}
	if((getSelectedPosition(m_selectedGradient.end) - event->pos())
	       .manhattanLength()
	   < 12)
	{
		movedColor = &m_selectedGradient.end;
	}

	if((event->buttons() & Qt::LeftButton) != 0u)
	{
		updateGradientFromMouse(event->pos());
	}
}

void LabGradientPicker::mouseMoveEvent(QMouseEvent* event)
{
	if((event->buttons() & Qt::LeftButton) != 0u)
	{
		updateGradientFromMouse(event->pos());
	}
}

QPoint LabGradientPicker::getSelectedPosition(grd::Color const& color) const
{
	const auto lab = color.getAs(grd::Color::Space::CIELAB);
	// Draw a circle to indicate the selected color
	const int selectedX = (lab[1] + 128.0) / 256.0 * width();
	const int selectedY = (lab[2] + 128.0) / 256.0 * height();

	return {selectedX, selectedY};
}

QPoint LabGradientPicker::getSelectedPosition(QVector2D const& control) const
{
	// Draw a circle to indicate the selected color
	const int selectedX = (control[0] + 128.0) / 256.0 * width();
	const int selectedY = (control[1] + 128.0) / 256.0 * height();

	return {selectedX, selectedY};
}

void LabGradientPicker::updateGradientFromMouse(QPoint const& pos)
{
	if(movedColor == nullptr && movedControl == nullptr)
	{
		return;
	}

	const int x = pos.x();
	const int y = pos.y();

	if(x >= 0 && x < width() && y >= 0 && y < height())
	{
		// Normalize the a* and b* values to fit within the widget
		const float a = (x / static_cast<double>(width())) * 256.0 - 128.0;
		const float b = (y / static_cast<double>(height())) * 256.0 - 128.0;

		if(movedColor != nullptr)
		{
			auto lab = movedColor->getAs(grd::Color::Space::CIELAB);
			lab[1]   = a;
			lab[2]   = b;
			movedColor->set(lab, grd::Color::Space::CIELAB);
		}
		else
		{
			*movedControl = {a, b};
		}
		emit gradientChanged(m_selectedGradient);

		// Repaint the widget
		update();
	}
}

QPoint bezierInterpolation(QPoint const& p0, QPoint const& p1, QPoint const& c0,
                           QPoint const& c1, float t)
{
	const float u   = 1.0 - t;
	const float tt  = t * t;
	const float uu  = u * u;
	const float uuu = uu * u;
	const float ttt = tt * t;

	QPoint p = uuu * p0;
	p += 3.0 * uu * t * c0;
	p += 3.0 * u * tt * c1;
	p += ttt * p1;

	return p;
}
