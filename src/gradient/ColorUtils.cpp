/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gradient/ColorUtils.hpp"

#include "gl/GLHandler.hpp"

#include <QJsonArray>
#include <QJsonObject>
#include <QVector3D>
#include <algorithm>
namespace grd
{

QVector2D bezierInterpolation(QVector2D const& p0, QVector2D const& p1,
                              QVector2D const& c0, QVector2D const& c1, float t)
{
	const float u   = 1.0 - t;
	const float tt  = t * t;
	const float uu  = u * u;
	const float uuu = uu * u;
	const float ttt = tt * t;

	QVector2D p = uuu * p0;
	p += 3.0 * uu * t * c0;
	p += 3.0 * u * tt * c1;
	p += ttt * p1;

	return p;
}

QVector3D linearTosRGB(QVector3D const& linear)
{
	QColor l(QColor::fromRgbF(linear[0], linear[1], linear[2]));
	l = GLHandler::linearTosRGB(l);
	QVector3D res(l.redF(), l.greenF(), l.blueF());
	return res;
}

QVector3D sRGBToLinear(QVector3D const& sRGB)
{
	QColor s(QColor::fromRgbF(sRGB[0], sRGB[1], sRGB[2]));
	s = GLHandler::sRGBToLinear(s);
	QVector3D res(s.redF(), s.greenF(), s.blueF());
	return res;
}

float hue2rgb(float p, float q, float t)
{
	if(t < 0.0f)
	{
		t += 1.0f;
	}
	if(t > 1.0f)
	{
		t -= 1.0f;
	}
	if(t < 1.0f / 6.0f)
	{
		return p + (q - p) * 6.0f * t;
	}
	if(t < 1.0f / 2.0f)
	{
		return q;
	}
	if(t < 2.0f / 3.0f)
	{
		return p + (q - p) * (2.0f / 3.0f - t) * 6.0f;
	}
	return p;
}

QVector3D HSLtosRGB(const QVector3D& hsl)
{
	const float h = hsl.x();
	const float s = hsl.y();
	const float l = hsl.z();

	if(s == 0.0f)
	{
		return {l, l, l};
	}

	const float q = (l < 0.5f) ? (l * (1.0f + s)) : (l + s - l * s);
	const float p = 2.0f * l - q;

	const float r = hue2rgb(p, q, h + 1.0f / 3.0f);
	const float g = hue2rgb(p, q, h);
	const float b = hue2rgb(p, q, h - 1.0f / 3.0f);

	return {r, g, b};
}

QVector3D sRGBToHSL(const QVector3D& rgb)
{
	const float r = rgb.x();
	const float g = rgb.y();
	const float b = rgb.z();

	const float maxVal = std::max(std::max(r, g), b);
	const float minVal = std::min(std::min(r, g), b);
	float h = NAN, s = NAN, l = NAN;

	l = (maxVal + minVal) / 2.0f;

	if(maxVal == minVal)
	{
		h = s = 0.0f; // Achromatic
	}
	else
	{
		const float d = maxVal - minVal;
		s             = (l > 0.5f) ? (d / (2.0f - maxVal - minVal))
		                           : (d / (maxVal + minVal));

		if(maxVal == r)
		{
			h = (g - b) / d + ((g < b) ? 6.0f : 0.0f);
		}
		else if(maxVal == g)
		{
			h = (b - r) / d + 2.0f;
		}
		else
		{
			h = (r - g) / d + 4.0f;
		}

		h /= 6.0f;
	}

	return {h, s, l};
}

QVector3D labToLinearRgb(QVector3D const& lab)
{
	const float L(lab[0]), a(lab[1]), b(lab[2]);

	// Convert Lab to XYZ
	double y = (L + 16.0) / 116.0;
	double x = a / 500.0 + y;
	double z = y - b / 200.0;

	x = 0.95047
	    * ((x * x * x > 0.008856) ? x * x * x : (x - 16.0 / 116.0) / 7.787);
	y = 1.00000
	    * ((y * y * y > 0.008856) ? y * y * y : (y - 16.0 / 116.0) / 7.787);
	z = 1.08883
	    * ((z * z * z > 0.008856) ? z * z * z : (z - 16.0 / 116.0) / 7.787);

	// Convert XYZ to RGB
	const float r  = x * 3.2406 + y * -1.5372 + z * -0.4986;
	const float g  = x * -0.9689 + y * 1.8758 + z * 0.0415;
	const float bl = x * 0.0557 + y * -0.2040 + z * 1.0570;

	return {r, g, bl};
}

QVector3D linearRGBToLab(QVector3D const& rgb)
{
	const float r  = rgb[0];
	const float g  = rgb[1];
	const float bl = rgb[2];

	// Convert RGB to XYZ
	float x = r * 0.4124564 + g * 0.3575761 + bl * 0.1804375;
	float y = r * 0.2126729 + g * 0.7151522 + bl * 0.0721750;
	float z = r * 0.0193339 + g * 0.1191920 + bl * 0.9503041;

	// Convert XYZ to Lab
	x /= 0.95047;
	y /= 1.00000;
	z /= 1.08883;

	x = (x > 0.008856) ? cbrt(x) : 7.787 * x + 16.0 / 116.0;
	y = (y > 0.008856) ? cbrt(y) : 7.787 * y + 16.0 / 116.0;
	z = (z > 0.008856) ? cbrt(z) : 7.787 * z + 16.0 / 116.0;

	const float L = 116.0 * y - 16.0;
	const float a = 500.0 * (x - y);
	const float b = 200.0 * (y - z);

	return {L, a, b};
}

Color::Color(QColor const& color)
{
	values = QVector3D(color.redF(), color.greenF(), color.blueF());
}

Color::Color(QVector3D const& values, Space space)
    : values(values)
    , space(space)
{
}

QVector3D Color::getAs(Space space) const
{
	if(space == this->space)
	{
		return values;
	}
	auto copy(*this);
	copy.convertTo(space);
	return copy.getAs(space);
}

QColor Color::getAsQColor() const
{
	const auto srgb = getAs(Space::SRGB);
	return QColor::fromRgbF(srgb[0], srgb[1], srgb[2]);
}

void Color::convertTo(Space space)
{
	if(space == this->space)
	{
		return;
	}

	// HSL converts to sRGB, not RGB
	if(space == Space::HSL)
	{
		if(this->space != Space::SRGB)
		{
			convertTo(Space::SRGB);
		}
		values      = sRGBToHSL(values);
		this->space = Space::HSL;
		return;
	}
	if(this->space == Space::HSL)
	{
		values      = HSLtosRGB(values);
		this->space = Space::SRGB;
		convertTo(space);
		return;
	}

	// always go through linear RGB; second condition breaks infinite recursion
	if(this->space != Space::RGB && space != Space::RGB)
	{
		convertTo(Space::RGB);
	}

	// now either the starting point or target is Space::RGB
	if(this->space == Space::RGB)
	{
		switch(space)
		{
			case Space::SRGB:
				values = linearTosRGB(values);
				break;
			case Space::CIELAB:
				values = linearRGBToLab(values);
				break;
			default:
				break;
		}
	}
	else
	{
		switch(this->space)
		{
			case Space::SRGB:
				values = sRGBToLinear(values);
				break;
			case Space::CIELAB:
				values = labToLinearRgb(values);
				break;
			default:
				break;
		}
	}
	this->space = space;
}

void Color::set(QVector3D const& values, Space space)
{
	this->values = values;
	this->space  = space;
}

QJsonObject Color::getJson() const
{
	QJsonObject result;
	result["values"] = QJsonArray({values.x(), values.y(), values.z()});
	result["space"]  = static_cast<int>(space);
	return result;
}

void Color::setJson(QJsonObject const& json)
{
	auto arr(json["values"].toArray());
	values = QVector3D(arr.at(0).toDouble(), arr.at(1).toDouble(),
	                   arr.at(2).toDouble());
	space  = static_cast<Space>(json["space"].toInt());
}

QJsonObject Gradient::getJson() const
{
	QJsonObject result;
	result["start"]  = start.getJson();
	result["c1"]     = QJsonArray({c1.x(), c1.y()});
	result["c2"]     = QJsonArray({c2.x(), c2.y()});
	result["end"]    = end.getJson();
	result["type"]   = static_cast<int>(type);
	result["grey"]   = grey;
	result["minVal"] = minVal;
	result["maxVal"] = maxVal;
	result["useLog"] = useLog;
	qDebug() << result;
	return result;
}

void Gradient::setJson(QJsonObject const& json)
{
	if(json.empty())
	{
		return;
	}
	start.setJson(json["start"].toObject());
	auto arr(json["c1"].toArray());
	c1  = QVector2D(arr.at(0).toDouble(), arr.at(1).toDouble());
	arr = json["c2"].toArray();
	c2  = QVector2D(arr.at(0).toDouble(), arr.at(1).toDouble());
	end.setJson(json["end"].toObject());
	type   = static_cast<Type>(json["type"].toInt());
	grey   = json["grey"].toBool();
	minVal = json["minVal"].toDouble();
	maxVal = json["maxVal"].toDouble();
	useLog = json["useLog"].toBool();
}

void Gradient::setShaderUniforms(GLShaderProgram const& shader) const
{
	Color::Space space(Color::Space::SRGB);
	switch(type)
	{
		case Gradient::Type::LINEAR_SRGB:
			space = Color::Space::SRGB;
			break;
		case Gradient::Type::LINEAR_RGB:
			space = Color::Space::RGB;
			break;
		case Gradient::Type::LINEAR_HSL:
			space = Color::Space::HSL;
			break;
		case Gradient::Type::LINEAR_CIELAB:
		case Gradient::Type::BEZIER_CIELAB:
			space = Color::Space::CIELAB;
			break;
	}
	shader.setUniform("startColor", start.getAs(space));
	shader.setUniform("c1", c1);
	shader.setUniform("c2", c2);
	shader.setUniform("endColor", end.getAs(space));
	shader.setUniform("interpolationType", static_cast<int>(type));
	shader.setUniform("limits", QVector2D(minVal, maxVal));
	shader.setUniform("useLog", useLog ? 1.f : 0.f);
	shader.setUniform("useGrey", grey ? 1.f : 0.f);
}

} // namespace grd
