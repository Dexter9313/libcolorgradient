/*
    Copyright (C) 2023 Florian Cabot <florian.cabot@hotmail.fr>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "gradient/GradientDisplay.hpp"

#include "QLabel"
#include <QPainter>
#include <QSizePolicy>
#include <random>

GradientDisplay::GradientDisplay(grd::Gradient const& gradient, QWidget* parent)
    : QWidget(parent)
    , gradient(gradient)
{
	setMinimumSize(200, 50);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
}

void GradientDisplay::paintEvent(QPaintEvent* event)
{
	QWidget::paintEvent(event);
	QPainter painter(this);

	// for random color noise
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> dis(0.0, 1.0);

	for(int x = 0; x < width(); x++)
	{
		const float t = (x / static_cast<float>(width()));

		const grd::Color color(evalGradient(gradient, t));

		auto rgb(color.getAs(grd::Color::Space::RGB));
		if(gradient.grey)
		{
			const QVector3D greyscale(0.2126, 0.7152, 0.0722);
			const float greyVal(QVector3D::dotProduct(greyscale, rgb));
			rgb = QVector3D(greyVal, greyVal, greyVal);
		}
		const auto srgb = grd::Color(rgb, grd::Color::Space::RGB)
		                      .getAs(grd::Color::Space::SRGB);

		const QColor qc(QColor::fromRgbF(srgb[0], srgb[1], srgb[2]));
		for(int y = 0; y < height(); y++)
		{
			if(srgb[0] < 0.0 || srgb[0] > 1.0 || srgb[1] < 0.0 || srgb[1] > 1.0
			   || srgb[2] < 0.0 || srgb[2] > 1.0)
			{
				// noise
				const QColor randomColor(
				    QColor::fromRgbF(dis(gen), dis(gen), dis(gen)));
				// painter.setPen(((y / 10) % 2 == 0) ? Qt::red : Qt::green);
				painter.setPen(randomColor);
			}
			else
			{
				painter.setPen(qc);
			}
			painter.drawPoint(x, y);
		}
	}
}

grd::Color GradientDisplay::evalGradient(grd::Gradient const& gradient, float t)
{
	switch(gradient.type)
	{
		case grd::Gradient::Type::LINEAR_SRGB:
		{
			const auto start(gradient.start.getAs(grd::Color::Space::SRGB));
			const auto end(gradient.end.getAs(grd::Color::Space::SRGB));
			return {(1.f - t) * start + t * end, grd::Color::Space::SRGB};
		}
		case grd::Gradient::Type::LINEAR_RGB:
		{
			const auto start(gradient.start.getAs(grd::Color::Space::RGB));
			const auto end(gradient.end.getAs(grd::Color::Space::RGB));
			return {(1.f - t) * start + t * end, grd::Color::Space::RGB};
		}
		case grd::Gradient::Type::LINEAR_HSL:
		{
			const auto start(gradient.start.getAs(grd::Color::Space::HSL));
			const auto end(gradient.end.getAs(grd::Color::Space::HSL));
			return {(1.f - t) * start + t * end, grd::Color::Space::HSL};
		}
		case grd::Gradient::Type::LINEAR_CIELAB:
		{
			const auto start(gradient.start.getAs(grd::Color::Space::CIELAB));
			const auto end(gradient.end.getAs(grd::Color::Space::CIELAB));
			return {(1.f - t) * start + t * end, grd::Color::Space::CIELAB};
		}
		case grd::Gradient::Type::BEZIER_CIELAB:
		{
			const auto startLab(
			    gradient.start.getAs(grd::Color::Space::CIELAB));
			const auto endLab(gradient.end.getAs(grd::Color::Space::CIELAB));

			const float resultL = startLab[0] * (1.f - t) + endLab[0] * t;

			const QVector2D resultab = grd::bezierInterpolation(
			    {startLab[1], startLab[2]}, {endLab[1], endLab[2]}, gradient.c1,
			    gradient.c2, t);

			return {QVector3D(resultL, resultab.x(), resultab.y()),
			        grd::Color::Space::CIELAB};
		}
		default:
			return {};
	}
}
